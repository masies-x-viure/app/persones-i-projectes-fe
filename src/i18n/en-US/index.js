// This is just an example,
// so you can safely delete all default props below

export default {
  sentences: [],
  failed: 'Action failed',
  success: 'Action was successful',
  next: 'next'
}
