import enUS from './en-US'
import ca from './ca'

export default {
  ca,
  'en-US': enUS
}
