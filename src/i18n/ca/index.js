// This is just an example,
// so you can safely delete all default props below

export default {
  sentences: [
    `El que necessita el poble és pa, que la llibertat vindrà per si sola quan es conquisti el dret a viure. La llibertat, la justícia, el dret res no signifiquen per al pobre, ni significaran res mentre, per menjar ell i la seva família,  necessitin dependre d'un amo. Quan la terra sigui del pobre llavors serà lliure, perquè deixarà de ser pobre. - Ricardo Flores Magón`,
    `Hem de crear el nostre propi món a les entranyes mateixes del món capitalista, però no sobre el paper i amb lirismes i elucubracions filosòfiques, sinó a més, sobre el terreny, pràcticament, despertant la veritable confiança en el nostre món d'avui i de demà - Joan Peiró, 1930.`,
    'Tota política que no fem nosaltres serà feta contra nosaltres - Joan Fuster',
    'Contra el vici de manar malament, hi ha la virtut de desobeir. - Lluís Maria Xirinacs',
    'Els petits canvis són poderosos - Capità enciam'
  ],
  success: 'Action was successful',
  next: 'següent'
}
