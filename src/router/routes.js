const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { name: 'home', path: '/', component: () => import('pages/IndexPage.vue') },
      { name: 'profileDetail', path: '/detail/:id', component: () => import('pages/UCDetail.vue') },
      {
        name: 'landingProfile',
        path: '/landing-profile',
        component: () => import('pages/LandingProfilePage.vue')
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    name: 'notFound',
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
