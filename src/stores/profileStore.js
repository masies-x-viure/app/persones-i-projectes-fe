import { defineStore } from 'pinia'

export const ProfileStore = defineStore('profile', {
  state: () => (
    {
      profileType: 'uc',
      stage: 'family',
      name: 'Família Rectoret - Carretas',
      descriptionShort:
      'Hola! Som 5 no-tant-joves (36-45a) i dues mainades(2,5 i 6 anys) que estem buscant una 4a Unitat de Convivència per dur a terme un projecte comunitari i permacultural arrelat al territori i a les seves entitats de l’econimia Social i Solidària i de cures vers persones de la Garrotxa.',
      members: [
        { name: 'Lou', age: 36, gender: 'male', profession: 'Web dev' },
        {
          name: 'Alba',
          age: 37,
          gender: 'female',
          profession: 'Educadora Social'
        }
      ],
      photos: [],
      locations: ['Garrotxa', 'Berguedà', 'Ripollès', 'Cerdanya'],
      questions: [],
      credentials: {
        pwd: 'Sibovv12!!',
        email: 'lourectoret@protonmail.com'
      }
    }),
  getters: {
    adults: (state) =>
      state.members.filter((member) => {
        return member.age >= 18
      }).length,
    underage: (state) =>
      state.members.filter((member) => {
        return member.age < 18
      }).length,
    profileTypeLabel: (state) => {
      switch (state.profileType) {
        case 'uc':
          return {
            label: {
              singular: 'Unitat de Convivència',
              plural: 'Unitats de Convivència'
            },
            sublabel: {
              0: 'Persona',
              1: 'Parella',
              2: 'Familia'
            }
          }
        case 'project':
          return {
            label: {
              singular: 'Projecte',
              plural: 'Projectes'
            },
            sublabel: {
              0: 'Projecte Llavor',
              1: 'Projecte en promoció',
              2: 'Projecte en convivència'
            }
          }
      }
    }
  }
})

// return {
//   sing: state.profileType === 'uc' ? 'Projecte' : 'Projecte',,
// Projectes:
//     state.profileType === 'uc' ? 'Unitats de Convivència' : 'Projectes'
// }
// }
