# Persones i Projectes FE (persones-i-projectes-fe)

Aplicació web per enxarxar les persones que vulguin habitar i produir en convivència amb la natura i les societats per a què siguin protagonistes del canvi de model productiu i habitacional.

## Install the dependencies

```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Lint the files

```bash
yarn lint
# or
npm run lint
```

### Build the app for production

1. Create .ENV file for production
2. Build

```bash
quasar build
```

### Customize the configuration

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).
